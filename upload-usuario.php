<?php
session_start();

$pasta_local = "upload/";
$caminho_imagem = $pasta_local . basename($_FILES["imagem"]["name"]);
$uploadok = 1;
$tipo_imagem = strtolower(pathinfo($caminho_imagem, PATHINFO_EXTENSION));
$novo_nome = $pasta_local . $_SESSION['cod_usuario'] . "." . $tipo_imagem;

// verificar se é uma imagem ou não 
if (isset($_POST['enviar'])) {
    $check = getimagesize($_FILES['imagem']['tmp_name']);
    if ($check !== false) {
        $uploadok = 1;
    } else {
        print "Arquivo não é uma imagem";
        $uploadok = 0;
    }
}

// //verificar s e o arquivo ja existe

// if(file_exists($caminho_imagem)){
//     print "Erro, arquivo já existente";
//     $uploadok = 0;

// }

//verificar o tamanho do arquivo => 1024 kb = 1mb

if($_FILES["imagem"]["size"] > 100000000) {
    print "Erro, arquivo muito grande";
    $uploadok = 0; 
}

// verificar tipo da imagem permitido 

if($tipo_imagem != "jpg" && $tipo_imagem != "png" && $tipo_imagem != "jpeg"){
print "Erro,permitido somente arquivos no formato jpg,png e jpeg";
$uploadok = 0;
    
}

if($uploadok == 0) {
    print "Erro, seu arquivo não foi aceito.";
} else {
    if(move_uploaded_file($_FILES["imagem"]["tmp_name"], $novo_nome)){
        include("conecta.php");
        $sql= "UPDATE tb_usuario
        SET foto_usuario ='$novo_nome'
        WHERE cod_usuario='".$_SESSION['cod_usuario']."'
        
        ";

        $res = mysqli_query($_con,$sql) or die("Não foi possivel atualizar
        o endereço da imagem");
        $_SESSION['foto_usuario']= null;
        $_SESSION['foto_usuario']= $novo_nome;
        $_SESSION['imagem_ok'] = "O arquivo".basename($_FILES['imagem']['name'])."foi atualizado";
        header("location:usuario.php");

    } else {
        print "Erro, não foi possivel fazer o upload <a href='usuario.php'>Voltar</a>";
    }
}