<?php
session_start();
include("verifica-logado.php");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    

    <?php include("menu2.php") ; ?>
    <title> Cadastro de animal perdido  </title>
    </head>

    <body>
    <fieldset class='mt-5'>
    <form>
    
    <h2>Cadastro de Animal Perdido </h2>
  <div class="form-row">
  <div class="col-md-6">
  <div class="custom-file">
    <input type="file" class="custom-file-input" id="imagem" name="imagem"/>
    <label class="custom-file-label" for="imagem"> Escolha uma imagem do animal</label>
    </div>
    </div>
    <div class="form-group col-md-6">
    <label for="inputAddress"> Qual o nome do animal? </label>
    <input type="text" class="form-control" id="inputAddress" placeholder="Escreva o nome do animal perdido">
  </div> 
    <div class="form-group col-md-6">
    <div class="form-group">
    <label for="exampleFormControlTextarea1">Descreva o animal (se usa remédios, se é agressivo, etc...)</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>  
  </div>
  <div class="row">
  <div class="form-group col-md-6">
    <label for="inputAddress"> Indique no mapa a localização de onde o animal foi visto pela última vez </label>
    <input type="text" class="form-control" id="inputAddress" placeholder="mapa">
  </div>
  </div>
  <div class="form-group col-md-4">
    <label for="inputAddress2"> Informe o endereço de onde o animal mora </label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="Escreva o endereço do animal">
  </div>
    <div class="form-group col-md-3">
      <label for="inputState"> Espécie </label>
      <select id="inputState" class="form-control">
        <option selected>Escolha a espécie...</option>
        <option>Cachorro</option>
        <option>Gato</option>
        <option>Outros </option>
      </select>
    </div>
    <div class="form-group col-md-3">
    <label for="inputAddress2"> Informe a raça do animal </label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="Escreva a raça do animal">
  </div>
  </div>
  <button type="submit" class="btn btn-primary">Cadastrar</button>
  
</form>
</fieldset>
    </body>
    </html>