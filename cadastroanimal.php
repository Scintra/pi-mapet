<?php
session_start();
include("verifica-logado.php");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    
    
    <?php include("menu2.php") ; ?>
    <title> Faça o cadastro de um animal  </title>
    </head>

    <body>
    <div class="container">
    <div class="row">
    <b>
    Faça o cadastro de um animal.</b>
    </div>
    <br>
    <br>
    
    <div class="row">
    <div class="col-sm-6" id="cadastroperdidos">
    <a href="cadastro-perdidos.php"><button type="button" 
        class="btn btn-primary btn-lg btn-block">Perdi um animal</button></a>
        </div>
    <div class="col-sm-6" id="cadastroabandonados">
        <a href="cadastro-abandonados.php"><button type="button"
        class="btn btn-primary btn-lg btn-block">Achei um animal abandonado</button>  </a>
   
    </div>
    </div>
    </div>
    </body>
    </html>