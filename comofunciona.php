<!DOCTYPE html>
<html lang="pt-br">
<head>
   
    
    <?php include("menu2.php") ; ?>
    
    <title>Como funciona?</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-sm-1">
</div>
<h2> Como funciona? </h1>
<div class="col-sm-10">
Através do site é possivel cadastrar um animal perdido ou abandonado. <p>
Veja um exemplo: Roberto perdeu seu cachorro chamado Rex e precisa encontrá-lo.
Ele então deve acessar o site, fazer o seu login ou se cadastrar.<br>
Após entrar no site, Roberto entra na aba <a href="cadastroanimal.php">"Cadastre um animal"</a>.
e clica na opção  <a href="cadastro-perdidos.php">"Perdi um animal"</a>
e preenche o formulário.<p>
Após clicar em cadastrar, o animal será exibido no site. <p>A exibição no inicio do site é aleatória.<p>
Para cadastrar um animal encontrado em situação de abandono segue-se os mesmos passos. Ao entrar na aba <br>
"Cadastre um animal", deve-se clicar na opção <a href="cadastro-abandonados.php"> Achei um animal abandonado</a>
e fazer o cadastro do animal.<br>
<p><b>
Lembrando que nosso site é gratuito e não cobramos nada para ajudar os animais.<p>
</div>
<div class="col-sm-1">
</div>
</div>
</div>

</body>
</html>