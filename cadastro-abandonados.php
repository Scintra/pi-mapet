<?php
session_start();
include("verifica-logado.php");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    
    
    <?php include("menu2.php") ; ?>
    <title> Cadastro de animal abandonado </title>
    </head>

    <body>
    <form>
    <h2>Cadastro de Animal Abandonado </h2>
  <div class="form-row">
  <div class="col-md-6">
  <div class="custom-file">
    <input type="file" class="custom-file-input" id="imagem" name="imagem"/>
    <label class="custom-file-label" for="imagem"> Escolha uma imagem</label>
    </div>
    </div>
    <div class="form-group col-md-12">
    <div class="form-group">
    <label for="exampleFormControlTextarea1">Descreva a condição do animal (se possui abrigo, comida, se está doente..</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>  
  </div>
  <div class="row">
  <div class="form-group col-md-12">
    <label for="inputAddress"> Indique no mapa a localização do animal </label>
    <input type="text" class="form-control" id="inputAddress" placeholder="Indque no mapa a localização do animal">
  </div>
  </div>
  <div class="form-group col-md-12">
    <label for="inputAddress2"> Informe o endereço do animal </label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="Escreva o endereço do animal">
  </div>
    <div class="form-group col-md-3">
      <label for="inputState"> Espécie </label>
      <select id="inputState" class="form-control">
        <option selected>Escolha a espécie...</option>
        <option>Cachorro</option>
        <option>Gato</option>
        <option>Outros </option>
      </select>
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Cadastrar</button>
</form>
    </body>
    </html>